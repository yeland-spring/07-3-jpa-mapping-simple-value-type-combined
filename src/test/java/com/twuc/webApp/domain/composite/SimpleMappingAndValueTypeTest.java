package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTypeTest extends JpaTestBase {
    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Test
    void should_get_and_save_company_profile() {
        ClosureValue<Long> companyProfileId = new ClosureValue<>();

        flushAndClear(em -> {
            final CompanyProfile companyProfile =
                    companyProfileRepository.save(new CompanyProfile(new Address("xian", "eight")));
            companyProfileId.setValue(companyProfile.getId());
        });

        run(em -> {
            final CompanyProfile companyProfile =
                    companyProfileRepository.findById(companyProfileId.getValue()).orElseThrow(RuntimeException::new);
            assertEquals("xian", companyProfile.getAddress().getCity());
            assertEquals("eight", companyProfile.getAddress().getStreet());
        });
    }

    @Test
    void should_get_and_save_user_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final UserProfile profile =
                    userProfileRepository.save(new UserProfile(new Address("xian", "tuanjie")));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final UserProfile profile =
                    userProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("xian", profile.getAddress().getCity());
            assertEquals("tuanjie", profile.getAddress().getStreet());
        });
    }
}
