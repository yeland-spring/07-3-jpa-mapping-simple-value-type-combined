package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue
    private Long id;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "city", column = @Column(name = "address_city", nullable = false, length = 128)),
        @AttributeOverride(name = "street", column = @Column(name = "address_street", nullable = false, length = 128)),
    })
    private Address address;

    public UserProfile() {
    }

    public UserProfile(Address address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }
}
